import pandas as pd
import matplotlib.pyplot as plt

l = [
    "Site",
    "Secteur",
    "Nb Secteurs",
    "X",
    "Y",
    "Unité Urbaine",
    "700 MHz",
    "800 MHz",
    "1800 MHz",
    "2100 MHz",
    "2600 MHz",
    "3500 MHz",
    "2023",
]


def import_data():
    secteur_dataFrame = pd.read_excel("base.xlsx")
    print(secteur_dataFrame)
    secteur_dataFrame.columns = l
    return secteur_dataFrame


def make_prevision(df):
    for i in range(2024, 2031):
        df[str(i)] = df.iloc[:, -1:] * 1.28
    return df


def rate_5g_calc(df):
    for i in range(len(rate_5g)):
        df["5G " + str(2023 + i)] = df[str(2023 + i)] * rate_5g[i] / 100
    return df


def rate_4g_calc(df):
    for i in range(len(rate_5g)):
        df["4G " + str(2023 + i)] = df[str(2023 + i)] * (100 - rate_5g[i]) / 100
    return df

#ajoute la colonne qui indique le type du secteur et la valeur du trafic saturant
def sature(df, seuil, bande):
    df1 = df.loc[df.groupby("Site")[bande].idxmax(), ["X", "Y", bande]]
    print(df1)
    df1.loc[df[bande] >= seuil, "Type"] = df[bande]
    df1.loc[df[bande] <= seuil, "Type"] = 0
    return df1


rate_5g = [21, 29, 37, 45, 53, 61, 69, 77]

#renvoie la base de donnée finale
def format_data():
    df = make_prevision(import_data())
    df = rate_5g_calc(df)
    df = rate_4g_calc(df)
    return (df)


#extrait la base et determine les positions saturés des sites pour 5G en 2030
df= format_data()
df1 = sature(df, 132, "5G 2030")
plot = df1.plot.scatter(x="X", y="Y", c="Type", title="Répartition des sites")
plt.show()
